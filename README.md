### Hi there!

----------------------------------

My name is Joseph. I am an aspiring embedded engineer attempting to improve my skills and learn new things.

### I’m currently working on ...

  I'm currently working on building a camera driver and an LCD driver from scratch for the STM32 platform, you can check out my       progress [here](https://gitlab.com/stm32-camera/)

  - Improving my C and C++ abilities
  - Learning about how OS's work
  - Learning about the inner workings of Linux
  - Learning about how WINE works and how I can help to improve it
  - Creating my own OS and making it publicly available 
  
### Plans for the future ... 

  - Learn more about kernel/OS development 
  - Learn more about driver development (Linux)
  - Help improve things related to the Pinephone Pro
  - Get patches merged into WINE
  - Get involved more with open source projects
  - Finish + publish unfinished projects

### Tech + Tools ... 
![OS](https://img.shields.io/badge/OS-Gentoo-purple) ![Text Editor](https://img.shields.io/badge/Text_Editor-vscodium-purple) ![Lang](https://img.shields.io/badge/Lang-C/C++-purple)
